# Guerrilla Mail API

[![Pub Version](https://img.shields.io/pub/v/guerrilla_mail_api?label=Published&logo=flutter&style=flat-square)](https://pub.dev/packages/guerrilla_mail_api)

This dart package enables you to communicate with Guerrilla Mail's temporary and anonymous mailing service.

## Features

- create disposable mail addresses
- fetch mails
- delete mails
- integrate into your own application

## Usage

See [`example`](example) for a full example.

## License

This project is EUPL licensed. For further details, consult [LICENSE](LICENSE).