import 'dart:io';

import 'package:guerrilla_mail_api/guerrilla_mail_api.dart';

void main() async {
  var client = GuerrillaMailApi();

  final response = await client.getEmailAddress();
  print('New address: ${response.emailAddress}\n\n');

  while (true) {
    await checkMails(client);
  }
}

Future<void> checkMails(GuerrillaMailApi client) async {
  print(
      'Press [ENTER] as soon as you received a mail.\nType an other mail user name to switch it.\nPress [CTRL] + [C] to stop the application.');

  final input = stdin.readLineSync();

  if (input != null && RegExp(r'^[\w\-\.\+]+$').hasMatch(input.trim())) {
    final response = await client.setEmailUser(input.trim());

    print('New address: ${response.emailAddress}\n\n');
    return;
  }

  final mails = await client.getEmailList();
  print("Got ${mails.count} mails:");

  if (mails.count > 0) {
    for (var mail in mails.list) {
      printMailExcerpt(mail, "${mails.list.indexOf(mail)}.");
    }
    print(
        '\n\nEnter the index of a mail to read the full message.\nPress [ENTER] to restart the application.');
  }

  final messageIdInput = stdin.readLineSync();

  if (messageIdInput != null &&
      RegExp(r'^\d+$').hasMatch(messageIdInput.trim())) {
    final id = int.parse(messageIdInput.trim());
    printFullMail(mails.list[id]);
  }
}

void printMailExcerpt(Message mail, String? prefix) {
  String message = '';
  if (prefix != null) message = '$prefix - ';
  message +=
      '${mail.subject} - ${mail.excerpt.replaceAll('\n', '\t')} - ${mail.date}';
  print(message);
}

void printFullMail(Message mail) {
  print(
      '${mail.subject} - ${mail.date} - sent by ${mail.from}\n\t\t---\n${mail.body}\n\t\t---\n\n');
}
