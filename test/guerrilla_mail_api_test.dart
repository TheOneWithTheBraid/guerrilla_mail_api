import 'package:guerrilla_mail_api/guerrilla_mail_api.dart';
import 'package:test/test.dart';

void main() {
  group('A group of tests', () {
    final client = GuerrillaMailApi();

    setUp(() {
      // Additional setup goes here.
    });

    test('Authenticating', () async {
      final result = await client.getEmailAddress();
      expect(result.auth?.success, isNull);
    });
  });
}
