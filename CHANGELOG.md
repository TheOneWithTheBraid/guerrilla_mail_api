## 1.0.1

- Made `Message` comparable

## 1.0.0

- Initial version.
