import 'dart:convert';

import 'package:guerrilla_mail_api/src/responses/guerrilla_mail_api_response.dart';
import 'package:http/http.dart';

/// the client for Guerrilla Mail
class GuerrillaMailApi {
  /// the locales officially supported by Guerrilla Mail
  static const locales = [
    'en',
    'fr',
    'nl',
    'ru',
    'tr',
    'uk',
    'ar',
    'ko',
    'jp',
    'zh',
    'zh-hant',
  ];

  static const errorCodes = {
    'auth-token-missing': 'Missing authentication token in the header',
    'auth-token-invalid': 'Invalid authentication token.',
    'auth-unknown-site': 'Invalid site used.',
    'auth-session-not-initialized':
        'Session not initialized. Please call get_email_address first to grab the session token',
    'auth-api-key-unconfigured': 'api key for site not generated'
  };

  /// the endpoint to access the API at
  /// default to the official Guerrilla Mail endpoint
  late final Uri apiEndpoint;

  /// the locale to use
  /// default to `en`
  /// must be one of [GuerrillaMailApi.locales]
  final String locale;

  GuerrillaMailApi({
    /// the endpoint to access the API at
    /// default to the official Guerrilla Mail endpoint
    Uri? apiEndpoint,
    this.locale = 'en',

    /// used to optionally import an existing session
    String? sessionId,
  }) {
    assert(locales.contains(locale),
        '`locale` must be one of $locales but is set to $locale.');
    if (sessionId != null) _phpsessid = sessionId;
    this.apiEndpoint =
        apiEndpoint ?? Uri.parse('https://api.guerrillamail.com/ajax.php');
  }

  /// the PHPSESSID provided by the Guerrilla Mail API
  String? _phpsessid;

  Future<Response> _doRequest({
    required String call,
    String userAgent = 'Flutter',
    String ip = '127.0.0.1',
    required Map<String, dynamic> queryParameters,
  }) async {
    queryParameters['f'] = call;
    queryParameters['ip'] = ip;
    queryParameters['agent'] = userAgent;
    if (_phpsessid != null) {
      queryParameters['sid_token'] = _phpsessid!;
    }
    final uri = Uri(
        scheme: apiEndpoint.scheme,
        host: apiEndpoint.host,
        path: apiEndpoint.path,
        queryParameters: queryParameters);
    final response = await get(uri);
    try {
      final json = jsonDecode(response.body);
      if (json is Map && json.containsKey('sid_token')) {
        _phpsessid = json['sid_token'];
      }
    } finally {}

    return response;
  }

  /// The function is used to initialize a session and set the client with an
  /// email address. If the session already exists, then it will return the
  /// email address details of the existing session. If a new session needs to
  /// be created, then it will create new email address randomly. The function
  /// will return a number of arguments for the client to remember, including
  /// the ‘sid_token’. The ‘sid_token’ is passed to each subsequent API call to
  /// maintain state.
  Future<GetEmailAddressResponse> getEmailAddress() async {
    final response =
        await _doRequest(call: 'get_email_address', queryParameters: {});
    return GetEmailAddressResponse.fromJson(jsonDecode(response.body));
  }

  /// Set the email address to a different email address.
  /// If the email has already been set, it will be given additional 60 minutes.
  /// Otherwise, a new email address will be generated if the email address is
  /// not in the database.
  Future<SetEmailUserResponse> setEmailUser(String emailUser) async {
    final response = await _doRequest(
        call: 'set_email_user', queryParameters: {'email_user': emailUser});
    return SetEmailUserResponse.fromJson(jsonDecode(response.body));
  }

  /// Check for new email on the server. Returns a list of the newest messages.
  /// The maximum size of the list is 20 items.
  ///
  /// The client should not check email too many times as to not overload the
  /// server. Do not check if the email expired, the email checking routing
  /// should pause if the email expired.
  ///
  /// [sequenceNumber] The sequence number (id) of the oldest email
  Future<CheckEmailResponse> checkEmail(
    /// The sequence number (id) of the oldest email
    int sequenceNumber,
  ) async {
    final response = await _doRequest(
        call: 'check_email',
        queryParameters: {'seq': sequenceNumber.toString()});
    return CheckEmailResponse.fromJson(jsonDecode(response.body));
  }

  /// Gets a maximum of 20 messages from the specified offset. Offset of 0 will
  /// fetch a list of the first 10 emails, offset of 10 will fetch a list of
  /// the next 10, and so on.
  ///
  /// This function is useful for populating the initial email list, if you
  /// want to check for new mail use check_mail. Note: When returned, subject
  /// and email excerpt are escaped using HTML Entities.
  Future<CheckEmailResponse> getEmailList({
    /// How many emails to start from (skip). Starts from 0
    int offset = 0,

    /// The sequence number (id) of the oldest email
    int? sequenceNumber,
  }) async {
    final response = await _doRequest(
      call: 'get_email_list',
      queryParameters: {
        'offset': offset.toString(),
        if (sequenceNumber != null) 'seq': sequenceNumber.toString(),
      },
    );
    return CheckEmailResponse.fromJson(jsonDecode(response.body));
  }

  /// Get the contents of an email.
  ///
  /// Notes:
  /// - All HTML in the body of the email is filtered. Eg, Javascript, applets,
  /// iframes, etc is removed.
  /// - All images in the email are relative to
  /// https://www.guerrillamail.com/res.php - this script will generate a
  /// `blocked by GM` image, indicating that the image was blocked. The CGI
  /// parameters for this script are as follows:
  ///     r = Is it a resource? 1 if true. Always 1
  ///     n= Node. The HTML element type. Can be a string of letters (a-z)
  ///     q= The query string for the original image. This is URL Encoded
  ///     Example URL:
  ///     https://www.guerrillamail.com/res.php?r=1&n=img&q=http%3A%2F%2Fstatic.groupon.de%2Fnewsletter_ums%2Flogo_groupon_de_DE.gif
  Future<Message> fetchEmail(
    /// The id of the email to fetch
    int id,
  ) async {
    final response = await _doRequest(
      call: 'fetch_email',
      queryParameters: {
        'email_id': id.toString(),
      },
    );
    return Message.fromJson(jsonDecode(response.body));
  }

  /// Forget the current email address. This will not stop the session, the
  /// existing session will be maintained. A subsequent call to
  /// get_email_address will fetch a new email address or the client can call
  /// set_email_user to set a new address. Typically, a user would want to set
  /// a new address manually after clicking the ‘forget me’ button.
  Future<bool?> forgetMe(String emailAddress) async {
    final response = await _doRequest(
      call: 'forget_me',
      queryParameters: {
        'email_id': emailAddress.toString(),
      },
    );
    return jsonDecode(response.body) as bool?;
  }

  /// Delete the emails from the server
  Future<DeleteEmailResponse> deleteEmail(
    /// the IDs of the emails to delete
    List<int> emailIds,
  ) async {
    final response = await _doRequest(
      call: 'fetch_email',
      queryParameters: {
        'email_ids': emailIds,
      },
    );
    return DeleteEmailResponse.fromJson(jsonDecode(response.body));
  }

  /// The idea of this function is to get some emails that are older (lower ID)
  /// than ‘seq’. How can this be useful? Say we have more than 1 page of
  /// emails. Then, we delete some emails from the first page, so we are left
  /// with some empty slots at the bottom. Instead of calling fetch_email_list,
  /// we call this to be more efficient and get exactly the number of email we
  /// need to fill.
  Future<GetEmailAddressResponse> getOlderList(
    /// get emails that have a lower id than the sequence
    int sequence,

    /// Integer how many emails to fetch max.
    int limit,
  ) async {
    final response = await _doRequest(
      call: 'get_older_list',
      queryParameters: {
        'seq': sequence,
        'limit': limit,
      },
    );
    return GetEmailAddressResponse.fromJson(jsonDecode(response.body));
  }
}
