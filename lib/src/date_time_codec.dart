import 'dart:convert';

/// a [Codec] used to serialize [DateTime] to and from [int]
class DateTimeCodec extends Codec<int, DateTime> {
  @override
  Converter<DateTime, int> get decoder => _DateTimeDecoder();

  @override
  Converter<int, DateTime> get encoder => _DateTimeEncoder();

  const DateTimeCodec();
}

class _DateTimeDecoder extends Converter<DateTime, int> {
  @override
  int convert(DateTime input) => (input.millisecondsSinceEpoch / 1000).round();

  const _DateTimeDecoder();
}

class _DateTimeEncoder extends Converter<int, DateTime> {
  @override
  DateTime convert(int input) =>
      DateTime.fromMillisecondsSinceEpoch(input * 1000);

  const _DateTimeEncoder();
}

const _jsonCodec = DateTimeCodec();

/// used by JsonSerializable to convert Guerrilla Mail's timestamps
DateTime intToDateTime(dynamic timestamp) =>
    _jsonCodec.encode(int.parse(timestamp.toString()));

/// used by JsonSerializable to convert Guerrilla Mail's timestamps
int dateTimeToInt(DateTime timestamp) => _jsonCodec.decode(timestamp);
