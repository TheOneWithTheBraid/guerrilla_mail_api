import 'package:equatable/equatable.dart';
import 'package:guerrilla_mail_api/guerrilla_mail_api.dart';
import 'package:json_annotation/json_annotation.dart';

import '../date_time_codec.dart';

part 'check_email.dart';
part 'delete_email_response.dart';
part 'get_email_address.dart';
part 'guerrilla_mail_api_response.g.dart';
part 'set_email_user.dart';

/// the base response model for many responses
abstract class GuerrillaMailApiResponse {
  final AuthResponse? auth;

  GuerrillaMailApiResponse(this.auth);

  Map<String, dynamic> toJson();
}
