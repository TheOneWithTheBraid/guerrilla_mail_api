part of 'guerrilla_mail_api_response.dart';

/// the response for [GuerrillaMailApi.getEmailAddress] requests.
@JsonSerializable()
class GetEmailAddressResponse extends GuerrillaMailApiResponse {
  /// The email address that that was determined. If a previous session was
  /// found, then it will be the email address of that session. A new random
  /// email address will be created.
  @JsonKey(name: 'email_addr')
  final String emailAddress;

  /// a UNIX timestamp when the email address was created. Used by the client
  /// to keep track of expiry.
  @JsonKey(
    name: 'email_timestamp',
    fromJson: intToDateTime,
    toJson: dateTimeToInt,
  )
  final DateTime emailTimestamp;

  /// This is the scrambled version of email_addr, eg “fcs5d3+vgdtknvsyt4”. The
  /// scrambled address is an alias for email_addr, so all email going to alias
  /// will arrive at the inbox of email_addr. Scrambled addresses are used to
  /// mask the email_addr, so it is difficult for someone else to know which
  /// email_addr was used.
  final String alias;

  /// Error message if the alias could not be set
  @JsonKey(name: 'alias_error')
  final String? aliasError;

  GetEmailAddressResponse({
    required this.emailAddress,
    required this.emailTimestamp,
    required this.alias,
    this.aliasError,
    AuthResponse? auth,
  }) : super(auth);

  factory GetEmailAddressResponse.fromJson(Map<String, dynamic> json) =>
      _$GetEmailAddressResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$GetEmailAddressResponseToJson(this);
}
