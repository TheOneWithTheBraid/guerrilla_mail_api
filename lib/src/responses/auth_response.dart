import 'package:guerrilla_mail_api/guerrilla_mail_api.dart';
import 'package:json_annotation/json_annotation.dart';

part 'auth_response.g.dart';

@JsonSerializable()
class AuthResponse {
  /// whether the authorization was successful
  final bool success;

  /// in case [success] is `false`, this may a [List] with some of the entries
  /// of [GuerrillaMailApi.errorCodes]
  @JsonKey(name: 'error_codes')
  final List<String> errorCodes;

  AuthResponse(this.success, this.errorCodes);

  factory AuthResponse.fromJson(Map<String, dynamic> json) =>
      _$AuthResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AuthResponseToJson(this);
}
