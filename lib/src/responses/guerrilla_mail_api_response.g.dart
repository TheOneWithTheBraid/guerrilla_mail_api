// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'guerrilla_mail_api_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckEmailResponse _$CheckEmailResponseFromJson(Map<String, dynamic> json) =>
    CheckEmailResponse(
      list: (json['list'] as List<dynamic>)
          .map((e) => Message.fromJson(e as Map<String, dynamic>))
          .toList(),
      count: _stringToInt(json['count']),
      email: json['email'] as String,
      timestamp: intToDateTime(json['ts']),
      users: json['users'] == null ? 1 : _stringToInt(json['users']),
      stats: Stats.fromJson(json['stats'] as Map<String, dynamic>),
      auth: json['auth'] == null
          ? null
          : AuthResponse.fromJson(json['auth'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CheckEmailResponseToJson(CheckEmailResponse instance) =>
    <String, dynamic>{
      'auth': instance.auth,
      'list': instance.list,
      'count': instance.count,
      'email': instance.email,
      'ts': dateTimeToInt(instance.timestamp),
      'users': instance.users,
      'stats': instance.stats,
    };

Message _$MessageFromJson(Map<String, dynamic> json) => Message(
      id: _stringToInt(json['mail_id']),
      from: json['mail_from'] as String,
      subject: json['mail_subject'] as String,
      excerpt: json['mail_excerpt'] as String,
      timestamp: intToDateTime(json['mail_timestamp']),
      read: _intToBool(json['mail_read']),
      date: json['mail_date'] as String,
      recipient: json['mail_recipient'] as String?,
      body: json['mail_body'] as String?,
      mimeType: json['content_type'] as String?,
    );

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'mail_id': _intToString(instance.id),
      'mail_from': instance.from,
      'mail_recipient': instance.recipient,
      'mail_subject': instance.subject,
      'mail_excerpt': instance.excerpt,
      'mail_body': instance.body,
      'content_type': instance.mimeType,
      'mail_timestamp': dateTimeToInt(instance.timestamp),
      'mail_read': _boolToInt(instance.read),
      'mail_date': instance.date,
    };

Stats _$StatsFromJson(Map<String, dynamic> json) => Stats(
      sequenceMail: _stringToInt(json['sequence_mail']),
      createdAddresses: _stringToInt(json['created_addresses']),
      receivedEmails: _stringToInt(json['received_emails']),
      total: _stringToInt(json['total']),
      totalPerHour: _stringToInt(json['total_per_hour']),
    );

Map<String, dynamic> _$StatsToJson(Stats instance) => <String, dynamic>{
      'sequence_mail': instance.sequenceMail,
      'created_addresses': instance.createdAddresses,
      'received_emails': instance.receivedEmails,
      'total': _intToString(instance.total),
      'total_per_hour': instance.totalPerHour,
    };

DeleteEmailResponse _$DeleteEmailResponseFromJson(Map<String, dynamic> json) =>
    DeleteEmailResponse(
      deletedIds:
          (json['deleted_ids'] as List<dynamic>).map((e) => e as int).toList(),
    );

Map<String, dynamic> _$DeleteEmailResponseToJson(
        DeleteEmailResponse instance) =>
    <String, dynamic>{
      'deleted_ids': instance.deletedIds,
    };

GetEmailAddressResponse _$GetEmailAddressResponseFromJson(
        Map<String, dynamic> json) =>
    GetEmailAddressResponse(
      emailAddress: json['email_addr'] as String,
      emailTimestamp: intToDateTime(json['email_timestamp']),
      alias: json['alias'] as String,
      aliasError: json['alias_error'] as String?,
      auth: json['auth'] == null
          ? null
          : AuthResponse.fromJson(json['auth'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GetEmailAddressResponseToJson(
        GetEmailAddressResponse instance) =>
    <String, dynamic>{
      'auth': instance.auth,
      'email_addr': instance.emailAddress,
      'email_timestamp': dateTimeToInt(instance.emailTimestamp),
      'alias': instance.alias,
      'alias_error': instance.aliasError,
    };

SetEmailUserResponse _$SetEmailUserResponseFromJson(
        Map<String, dynamic> json) =>
    SetEmailUserResponse(
      emailAddress: json['email_addr'] as String,
      emailTimestamp: intToDateTime(json['email_timestamp']),
      alias: json['alias'] as String,
      site: json['site'] as String?,
      siteId: _stringToInt(json['site_id']),
      aliasError: json['alias_error'] as String?,
    );

Map<String, dynamic> _$SetEmailUserResponseToJson(
        SetEmailUserResponse instance) =>
    <String, dynamic>{
      'email_addr': instance.emailAddress,
      'email_timestamp': dateTimeToInt(instance.emailTimestamp),
      'alias': instance.alias,
      'alias_error': instance.aliasError,
      'site': instance.site,
      'site_id': instance.siteId,
    };
