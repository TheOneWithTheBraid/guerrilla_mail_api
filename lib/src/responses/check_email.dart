part of 'guerrilla_mail_api_response.dart';

/// the response for [GuerrillaMailApi.checkEmail] requests.
@JsonSerializable()
class CheckEmailResponse extends GuerrillaMailApiResponse {
  /// A list of messages, represented as an array of objects.
  final List<Message> list;

  /// The number of emails matched. This can be much more than the maximum that
  /// can be returned (20)! This number indicates the total number of new
  /// emails in the database. (If you want to get the emails after the first
  /// 20, then make another call with the get_older_list function
  @JsonKey(fromJson: _stringToInt)
  final int count;

  /// The email address which the list is for. Eg. test@guerrillamailblock.com
  /// The client would need to watch this variable to detect any changes to
  /// sync the address.
  final String email;

  /// The timestamp of the email address, time of when the email address was
  /// created. The client can use this variable to sync the time. Emails expire
  /// after 60 minutes
  @JsonKey(name: 'ts', fromJson: intToDateTime, toJson: dateTimeToInt)
  final DateTime timestamp;

  /// the number of users viewing this inbox (not exact)
  @JsonKey(fromJson: _stringToInt)
  final int? users;

  final Stats stats;

  CheckEmailResponse({
    required this.list,
    required this.count,
    required this.email,
    required this.timestamp,
    this.users = 1,
    required this.stats,
    AuthResponse? auth,
  }) : super(auth);

  factory CheckEmailResponse.fromJson(Map<String, dynamic> json) =>
      _$CheckEmailResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$CheckEmailResponseToJson(this);
}

/// the implementation of an email message
@JsonSerializable()
class Message extends Equatable {
  /// an integer
  @JsonKey(name: 'mail_id', fromJson: _stringToInt, toJson: _intToString)
  final int id;

  /// email address of sender
  @JsonKey(name: 'mail_from')
  final String from;

  /// email address of receiver
  @JsonKey(name: 'mail_recipient')
  final String? recipient;

  /// UTF-8 encoded string of the email’s subject
  @JsonKey(name: 'mail_subject')
  final String subject;

  /// a short excerpt of the email
  @JsonKey(name: 'mail_excerpt')
  final String excerpt;

  /// The message part of the email. May contain filtered HTML as described above
  @JsonKey(name: 'mail_body')
  final String? body;

  /// MIME type for the mail_body, can be text/plain or text/html
  @JsonKey(name: 'content_type')
  final String? mimeType;

  /// unix timestamp of arrival
  @JsonKey(
    name: 'mail_timestamp',
    fromJson: intToDateTime,
    toJson: dateTimeToInt,
  )
  final DateTime timestamp;

  /// indicating if email was fetched before
  @JsonKey(name: 'mail_read', fromJson: _intToBool, toJson: _boolToInt)
  final bool read;

  /// Date of arrival in the following format if less than 24 hours: H:M:S, or Y-M-D if older than 24
  @JsonKey(name: 'mail_date')
  final String date;

  Message({
    required this.id,
    required this.from,
    required this.subject,
    required this.excerpt,
    required this.timestamp,
    required this.read,
    required this.date,
    this.recipient,
    this.body,
    this.mimeType,
  });

  factory Message.fromJson(Map<String, dynamic> json) =>
      _$MessageFromJson(json);

  Map<String, dynamic> toJson() => _$MessageToJson(this);

  @override
  List<Object?> get props => [id];
}

/// some pretty stats provided by the API to tell the users how much Guerrilla
/// Mail is being used
@JsonSerializable()
class Stats {
  @JsonKey(name: 'sequence_mail', fromJson: _stringToInt)
  final int sequenceMail;
  @JsonKey(name: 'created_addresses', fromJson: _stringToInt)
  final int createdAddresses;

  @JsonKey(name: 'received_emails', fromJson: _stringToInt)
  final int receivedEmails;

  @JsonKey(fromJson: _stringToInt, toJson: _intToString)
  final int total;

  @JsonKey(name: 'total_per_hour', fromJson: _stringToInt)
  final int totalPerHour;

  Stats({
    required this.sequenceMail,
    required this.createdAddresses,
    required this.receivedEmails,
    required this.total,
    required this.totalPerHour,
  });

  factory Stats.fromJson(Map<String, dynamic> json) => _$StatsFromJson(json);

  Map<String, dynamic> toJson() => _$StatsToJson(this);
}

/// used for JSON serialization
bool _intToBool(dynamic input) => double.parse(input.toString()) == 1;

/// used for JSON serialization
int _boolToInt(bool input) => input ? 1 : 0;

/// used for JSON serialization
int _stringToInt(dynamic input) =>
    int.parse(input.toString().replaceAll(RegExp(r'\D'), ''));

/// used for JSON serialization
String _intToString(int input) => input.toString();
