part of 'guerrilla_mail_api_response.dart';

/// the response for [GuerrillaMailApi.deleteEmail] requests.
@JsonSerializable()
class DeleteEmailResponse extends GuerrillaMailApiResponse {
  @JsonKey(name: 'deleted_ids')
  final List<int> deletedIds;

  DeleteEmailResponse({required this.deletedIds, AuthResponse? authResponse})
      : super(authResponse);

  factory DeleteEmailResponse.fromJson(Map<String, dynamic> json) =>
      _$DeleteEmailResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$DeleteEmailResponseToJson(this);
}
