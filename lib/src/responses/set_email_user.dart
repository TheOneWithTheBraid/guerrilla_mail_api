part of 'guerrilla_mail_api_response.dart';

/// the response for [GuerrillaMailApi.setEmailUser] requests.
@JsonSerializable()
class SetEmailUserResponse extends GetEmailAddressResponse {
  /// The identifier of the site matched
  final String? site;

  /// The ID of the site which the domain belongs to
  @JsonKey(name: 'site_id', fromJson: _stringToInt)
  final int? siteId;

  SetEmailUserResponse({
    required String emailAddress,
    required DateTime emailTimestamp,
    required String alias,
    this.site,
    this.siteId,
    String? aliasError,
  }) : super(
          emailAddress: emailAddress,
          emailTimestamp: emailTimestamp,
          alias: alias,
          aliasError: aliasError,
        );

  factory SetEmailUserResponse.fromJson(Map<String, dynamic> json) =>
      _$SetEmailUserResponseFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$SetEmailUserResponseToJson(this);
}
