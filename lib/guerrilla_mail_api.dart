/// The client library to access Guerrilla Mail's API.
///
/// You most likely want to access [GuerrillaMailApi].
///

library guerrilla_mail_api;

import 'package:guerrilla_mail_api/src/guerrilla_mail_api_base.dart';

export 'src/guerrilla_mail_api_base.dart';
export 'src/responses/auth_response.dart';
export 'src/responses/guerrilla_mail_api_response.dart';
